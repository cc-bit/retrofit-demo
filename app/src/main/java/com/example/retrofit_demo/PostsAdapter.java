package com.example.retrofit_demo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostsAdapterViewHold> {

    private List<Post> postList;
    private Context context;

    private ClickedItem clickedItem;

    /**
     * This will bring the passed clicked item into the class
     * @param clickedItem
     */
    public PostsAdapter(ClickedItem clickedItem) {
        this.clickedItem = clickedItem;
    }

    /**
     * This will bring the passed post list inside the class.
     * @param postList
     */
    public void setData(List<Post> postList) {
        this.postList = postList;
        notifyDataSetChanged();
    }

    /**
     * The view holder for the adapter
     */
    public class PostsAdapterViewHold extends RecyclerView.ViewHolder {


        TextView title;
        TextView prefix;
        ImageView imageArrow;

        /**
         * Pass through the values that will used when building the recyclerview.
         * @param itemView
         */
        public PostsAdapterViewHold(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            prefix = itemView.findViewById(R.id.prefix);
            imageArrow = itemView.findViewById(R.id.imageArrow);

        }
    }

    /**
     * When creating the view holder, find and attach to the rows_of_posts activity.
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public PostsAdapter.PostsAdapterViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new PostsAdapter.PostsAdapterViewHold(LayoutInflater.from(context).inflate(R.layout.rows_of_posts,parent,false));
    }

    /**
     * Looks into the post list at an index and pulls a title.  Prefix will display the type of file.  It's only "Text" for now.
     * This also handles the onClick method for the clicked item.
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull PostsAdapter.PostsAdapterViewHold holder, int position) {
        Post post = postList.get(position);
        String title = post.getTitle();
        String prefix = "Text";
        holder.prefix.setText(prefix);
        holder.title.setText(title);
        holder.imageArrow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                clickedItem.ClickedPost(post);
            }
        });
    }

    /**
     * Gives the size the recyclerview should be to improve performance.
     * @return
     */
    @Override
    public int getItemCount() {
        return postList.size();
    }

    /**
     * Clicked item interface.
     */
    public interface ClickedItem {
        public void ClickedPost(Post post);
    }
}
