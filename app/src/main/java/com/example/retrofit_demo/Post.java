package com.example.retrofit_demo;

public class Post {
    private int id; // The id while in the given api route
    private String text; // The text while in the given api route



    public int getId() {
        return id;
    }

    public String getTitle() {
        return text;
    }


    /**
     * a toString method written for debugging.
     * @return
     */
    @Override
    public String toString() {
        return "Post{" +
                ", id=" + id +
                ", title='" + text + '\'' +
                '}';
    }
}
