package com.example.retrofit_demo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import androidx.appcompat.widget.Toolbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements PostsAdapter.ClickedItem{

    Toolbar toolbar;
    RecyclerView recyclerView;

    PostsAdapter postsAdapter;

    /**
     * Creatinon of activity.  Builds a recycler view.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recyclerview);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

        postsAdapter = new PostsAdapter(this::ClickedPost);

        getPosts();

    }

    /**
     * Run the call and use the callback.
     */
    public void getPosts(){
        Call<List<Post>> postList = ApiClient.getService().getPosts();

        postList.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
               // Check that the call was a success
                if(response.isSuccessful()){
                    List<Post> postResponse = response.body();

                    postsAdapter.setData(postResponse);

                    recyclerView.setAdapter(postsAdapter);
                   // Log.e("success", response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.e("failure", t.getLocalizedMessage()); // In case of failure.
            }
        });
    }

    /**
     * Launch an activity for a post's text body on a click event.
     * @param post
     */
    @Override
    public void ClickedPost(Post post) {
        //Log.e("clicked on post",post.getTitle());
        Intent intent = new Intent(this,TextBodyAct.class).putExtra("selection",post.getId());
        startActivity(intent);
    }
}