package com.example.retrofit_demo;

public class Text {
    private int id;
    private String text;



    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }



    @Override
    public String toString() {
        return "Post{" +
                ", id=" + id +
                ", title='" + text + '\'' +
                '}';
    }
}
