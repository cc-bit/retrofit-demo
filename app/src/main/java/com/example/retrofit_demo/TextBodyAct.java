package com.example.retrofit_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TextBodyAct extends AppCompatActivity {

    public static int passedArg = 0;  // Extra used to select a Post's text body when making another service call.
    TextView textView;

    /**
     * Creation of Activity.
     * Obtains the Extra passedArg from the main activity with the key, "selection".
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_body);

        textView = findViewById(R.id.body);

        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            passedArg = intent.getIntExtra("selection",0);
        }

        getClickedText();



//        url = "http://my-json-server.typicode.com/chrismchitwood/json-example/text" + passedArg + "/";
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(url)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        // Here, retrofit creates the body of code for the new object.
//        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
//
//        Call<List<Text>> call = jsonPlaceHolderApi.getText();
//
//        call.enqueue(new Callback<List<Text>>() {
//            @Override
//            public void onResponse(Call<List<Text>> call, Response<List<Text>> response) {
//
//                if(!response.isSuccessful()){
//                    textView.setText("Connection Error\nCode: " + response.code());
//                    return;
//                }
//
//                List<Text> texts = response.body();
//
//                for (Text text : texts) {
//
//
//                    textView.setText(text.getText());
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Text>> call, Throwable t) {
//                textView.setText(t.getMessage());
//            }
//        });
    }

    /**
     * Use retrofit to make a second service call to grab the clicked post's text body.
     * The text displayed in the textview will simply be "Excerpt" until then.
     */
    public void getClickedText(){
        Call<List<Post>> postList = ApiClient.getService().getTexts();

        postList.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    List<Post> postResponse = response.body();

                    String result = postResponse.get(passedArg).getTitle();

                    textView.setText(result);

                    // Log.e("success", response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.e("failure", t.getLocalizedMessage());
            }
        });
    }
}