package com.example.retrofit_demo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Service {

    // For the post titles
    @GET("posts")
    Call<List<Post>> getPosts();


    // For the actual texts.
    @GET("texts")
    Call<List<Post>> getTexts();


}
